<div align="center">
	<img src="assets/dots.png" width="200" />
	<h1 align="center">poimandres-helix</h1>
</div>

A port of the beautiful [poimandres][poimandres] theme to [helix][helix].

This is a work-in-progress, so some features may be missing or broken.

## Screenshots

<div align="center">
  <img src="assets/screenshot.png">
</div>

## Install
Copy `poimandres.toml` from the `themes` folder to your helix theme path, e.g. `${HOME}/.config/helix/themes` (you may need to create this directory).
Then, simply load the theme in your helix config (`${HOME}/.config/helix/config.toml`):
```
theme = "poimandres"
```

## Related
- [poimandres-theme][poimandres]: The original VS Code theme this is based on
- [poimandres-alacritty][poimandres-alacritty]: For Alacritty (this Markdown is based on the one here)
- [poimandres-nvim][poimandres-nvim]: For Neovim. Many of the tree-sitter definitions used here were drawn from the Neovim port

[poimandres]: https://github.com/drcmda/poimandres-theme
[helix]: https://helix-editor.com
[poimandres-alacritty]: https://github.com/z0al/poimandres-alacritty
[poimandres-nvim]: https://github.com/olivercederborg/poimandres.nvim